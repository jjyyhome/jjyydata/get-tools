#!/usr/bin/env bash

git submodule init

git submodule add https://github.com/kkoojjyy/bookmark-archiver.git  ../bookmark-archiver

git submodule add https://github.com/kkoojjyy/ovirt-engine.git ../ovirt-engine

git submodule add https://github.com/kkoojjyy/FreshRSS.git ../freshrss

git submodule add https://github.com/pirate/bookmark-archiver.git   ../bookmark-archiver

git submodule add https://github.com/kkoojjyy/beakerx.git ../beakerx

git submodule update